package com.karthi.androidlearn;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.distribute.Distribute;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCenter.start((Application) getApplicationContext(),"f0af38906c011cb8e4830972b256cea0fc0f854f",Distribute.class);
    }
}
